﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisitorHomeTask.InfoTypes;

namespace VisitorHomeTask
{
    class Program
    {
        static void Main(string[] args)
        {
            var errorHandler = new ErrorHandler();

            var amazonServerError = new ServerErrorInfo() { Severity = Severity.Critical, Description = "Amazon", TimeStamp = DateTime.Today, Id = Guid.NewGuid(), ServerId = Guid.NewGuid() };
            var microsoftServerError = new ServerErrorInfo() { Severity = Severity.High, Description = "Microsoft", TimeStamp = DateTime.Today, Id = Guid.NewGuid(), ServerId = Guid.NewGuid() };
            var googleServerError = new ServerErrorInfo() { Severity = Severity.Low, Description = "Google", TimeStamp = DateTime.Today, Id = Guid.NewGuid(), ServerId = Guid.NewGuid() };

            var amazonDatabaseError = new DatabaseErrorInfo() { Severity = Severity.Critical, Description = "Amazon", TimeStamp = DateTime.Today, DatabaseName = "Servers", Id = Guid.NewGuid(), UserId = Guid.NewGuid() };
            var microsoftDatabaseError = new DatabaseErrorInfo() { Severity = Severity.High, Description = "Microsoft", TimeStamp = DateTime.Today, DatabaseName = "Licenses", Id = Guid.NewGuid(), UserId = Guid.NewGuid() };
            var googleDatabaseError = new DatabaseErrorInfo() { Severity = Severity.Low, Description = "Google", TimeStamp = DateTime.Today, DatabaseName = "Users", Id = Guid.NewGuid(), UserId = Guid.NewGuid() };

            errorHandler.Add(amazonServerError);
            errorHandler.Add(microsoftServerError);
            errorHandler.Add(googleServerError);

            errorHandler.Add(amazonDatabaseError);
            errorHandler.Add(microsoftDatabaseError);
            errorHandler.Add(googleDatabaseError);

            errorHandler.Accept(new ShowInfoInColumn());
            errorHandler.Accept(new ShowInfoInRow());

            Console.ReadKey();
        }
    }
}
