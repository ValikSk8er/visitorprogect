﻿using System.Collections.Generic;
using VisitorHomeTask.InfoTypes;

namespace VisitorHomeTask
{
    public class ErrorHandler
    {
        private List<ErrorInfo> Errors = new List<ErrorInfo>();

        public void Accept(Visitor visitor)
        {
            foreach (ErrorInfo error in Errors)
            {
                error.Accept(visitor);
            }
        }

        public void Add(ErrorInfo errorInfo)
        {
            Errors.Add(errorInfo);
        }
    }
}
