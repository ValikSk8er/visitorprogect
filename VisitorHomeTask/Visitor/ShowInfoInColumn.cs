﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisitorHomeTask.InfoTypes;

namespace VisitorHomeTask
{
    public class ShowInfoInColumn : Visitor
    {
        public override void LogDatabaseInfo(DatabaseErrorInfo errorInfo)
        {
            Console.WriteLine($"| Log Database Info:" +
                $"\n| Id           : {errorInfo.Id}" +
                $"\n| Description  : {errorInfo.Description}" +
                $"\n| TimeStamp    : {errorInfo.TimeStamp}" +
                $"\n| Severity     : {errorInfo.Severity}" +
                $"\n| DatabaseName : {errorInfo.DatabaseName}" +
                $"\n| UserId       : {errorInfo.UserId}");
            Console.WriteLine(new string('-', 55));
        }

        public override void LogServerInfo(ServerErrorInfo errorInfo)
        {
            if (errorInfo.Severity == Severity.Critical)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            Console.WriteLine($"| Log Server Info:" +
                $"\n| Id           : {errorInfo.Id}" +
                $"\n| Description  : {errorInfo.Description}" +
                $"\n| TimeStamp    : {errorInfo.TimeStamp}" +
                $"\n| Severity     : {errorInfo.Severity}" +
                $"\n| ServerId     : {errorInfo.ServerId}");

            Console.WriteLine(new string('-', 55));
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
