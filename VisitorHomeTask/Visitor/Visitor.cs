﻿using VisitorHomeTask.InfoTypes;

namespace VisitorHomeTask
{
    public abstract class Visitor
    {
        public abstract void LogServerInfo(ServerErrorInfo serverErrorInfo);
        public abstract void LogDatabaseInfo(DatabaseErrorInfo databaseErrorInfo);
    }
}
