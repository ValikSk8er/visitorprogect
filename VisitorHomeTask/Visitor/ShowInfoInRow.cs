﻿using System;
using VisitorHomeTask.InfoTypes;

namespace VisitorHomeTask
{
    public class ShowInfoInRow : Visitor
    {
        public override void LogDatabaseInfo(DatabaseErrorInfo errorInfo)
        {
            Console.WriteLine($"Log Database Info:" +
                $"\nId : {errorInfo.Id};" +
                $" Description : {errorInfo.Description};" +
                $" TimeStamp : {errorInfo.TimeStamp};" +
                $"\nSeverity : {errorInfo.Severity};" +
                $" DatabaseName : {errorInfo.DatabaseName};" +
                $" UserId : {errorInfo.UserId}");
            Console.WriteLine(new string('-', 55));
        }

        public override void LogServerInfo(ServerErrorInfo errorInfo)
        {
            if (errorInfo.Severity == Severity.Critical)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }

            Console.WriteLine($"Log Server Info:" +
                $"\nId : {errorInfo.Id};" +
                $" Description : {errorInfo.Description};" +
                $" TimeStamp : {errorInfo.TimeStamp};" +
                $"\nSeverity : {errorInfo.Severity};" +
                $" ServerId : {errorInfo.ServerId};");

            Console.WriteLine(new string('-', 55));
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
