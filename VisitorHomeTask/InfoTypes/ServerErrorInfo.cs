﻿using System;

namespace VisitorHomeTask.InfoTypes
{
    public class ServerErrorInfo : ErrorInfo
    {
        public Guid ServerId { get; set; }

        public override void Accept(Visitor visitor)
        {
            visitor.LogServerInfo(this);
        }
    }
}
