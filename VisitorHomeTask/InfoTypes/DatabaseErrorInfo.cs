﻿using System;

namespace VisitorHomeTask.InfoTypes
{
    public class DatabaseErrorInfo : ErrorInfo
    {
        public string DatabaseName { get; set; }
        public Guid UserId { get; set; }

        public override void Accept(Visitor visitor)
        {
            visitor.LogDatabaseInfo(this);
        }
    }
}
