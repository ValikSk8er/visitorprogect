﻿using System;

namespace VisitorHomeTask.InfoTypes
{
    public abstract class ErrorInfo
    {
        public Guid Id { get; set; }
        public string Description { get; set; }
        public DateTime TimeStamp { get; set; }
        public Severity Severity { get; set; }

        public abstract void Accept(Visitor visitor);
    }

    public enum Severity
    {
        Low,
        High,
        Critical
    }
}
